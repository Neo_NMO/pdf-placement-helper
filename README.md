# PDF Placement Helper #
PDF Placement helper, so you don't have to measure things out the hard way.

### What is this repository for? ###

* PDF Y axis is backwards, so this allows you to place a box on the document, and get the dimensions.
* 0.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Restore packages
* ???
* Profit

### Contribution guidelines ###

* //TODO

### Who do I talk to? ###

* Josh Briggs (<josh@briggsdev.us>)